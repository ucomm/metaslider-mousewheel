<?php
/*
Plugin Name: Metaslider Mousewheel
Description: Adds support for mousewheeling on metaslider.
Author: UComm Web Team
Version: 1.0.0
Text Domain: metaslider-mousewheel
*/

class MetasliderMousewheel {

    public function __construct() {
        $this->version = '1.0.0';
    }

    public function metaslider_flex_params($options, $slider_id, $settings) {
        $options['start'][] = "var timer = null;var wheeling = false;$('.flexslider').on('wheel', function(event){var deltaY = event.originalEvent.deltaY;if(timer){clearTimeout(timer);}if(!wheeling){var target = deltaY < 0 ? slider.getTarget('next') : slider.getTarget('prev');slider.flexAnimate(target, true);}wheeling = true;timer = setTimeout(function(){wheeling = false;}, 60);});";
        return $options;
    }

    public function run() {
        add_filter('metaslider_flex_slider_parameters', 'metaslider_flex_params', 10, 3);
    }
    
}

$metaslider_mousewheel = new MetasliderMousewheel();
$metaslider_mousewheel->run();